<?php

namespace App\Http\Controllers\Calculators;

class Calculator
{
    // addition logic
    public function add($a, $b)
    {
        return $a + $b;
    }

    // subtraction logic
    public function subtract($a, $b)
    {
        return $a - $b;
    }

    public function multiply($a, $b)
    {
        return $a * $b;
    }

    public function division($a, $b)
    {
        return $a / $b;
    }




 }