<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Http\Controllers\Calculators\Calculator;

//class CalculatorTest extends PHPUnit\Framework\TestCase
class CalculatorTest extends TestCase
{

    private $calculator;


    // (1) - create an empty array 
    public function testEmpty()    
    {
        $stack = [];
        $this->assertEmpty($stack);        
        return $stack;    
    }

    // (2) - push some stuff onto stack 'foo'
    /**     * @depends testEmpty     */    
    public function testPush(array $stack)   
    {        
        array_push($stack, 'foo');        
        $this->assertEquals('foo', $stack[count($stack)-1]);        
        $this->assertNotEmpty($stack);
        return $stack;    
    }

    // (3) - pop stuff off the stack and 
    // check that stack is empty after 'pop' operation 
    /**     * @depends testPush     */    
    public function testPop(array $stack)    
    {        
        $this->assertEquals('foo', array_pop($stack));       
        $this->assertEmpty($stack);    
    } 

    // (4) - create instance of Calculator & 
    // confirm that 'calculator' is in fact 
    // an instance of class Calculator
    protected function setUp()
    {
        $this->calculator = new Calculator();
        //check that 'calculator' is an instance of expected 'Calculator' class
        $this->assertInstanceOf(Calculator::class, $this->calculator);
    }
    
    // (5) - destructor
    protected function tearDown()
    {
        $this->calculator = NULL;
    }

    // (6) Calculator - test add function
    public function testAdd()
    {
        $result = $this->calculator->add(1, 2);
        $this->assertEquals(3, $result);
        //dd("blah"); exit;
    }  

    // (7) Calculator - test subtract function
    public function testSubtract()
    {
        $result = $this->calculator->subtract(5, 2);
        $this->assertEquals(3, $result);
        //dd("blah"); exit;
    }


}